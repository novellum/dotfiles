if [ $1 = '' ] ; then
    format='hello'
else
    format="$1"
fi

scanimage --format=$format > ~/Images/scans/scan_$(date +"%Y_%m_%d_%H_%M_%S").$format