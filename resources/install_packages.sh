#!/bin/sh
echo "Installing needed packages"
list=""

## Making sure yay is installed
sudo pacman -S --needed yay neofetch

## Old packages
# dunst 
# nerd-fonts-complete
# ttf-hack \

## Sway packages
pack="sway \
swaylock \
swayidle \
libcurl-compat \
waybar \
qt5-wayland \
qgnomeplatform-git \
rofi \
zsh \
oh-my-zsh-git \
kitty \
i3status-rust-git \
wdisplays-git \
wlogout \
mako-git"

## Interface controlls
pack="$pack \
redshift-wlr-gamma-control-git \
light \
grim \
slurp \
wl-clipboard \
ydotool-git \
xbindkeys"

## Audio packages
pack="$pack \
playerctl \
alsa-utils \
pulseaudio-alsa \
pamixer"

## Security
pack="$pack \
gnome-keyring \
libsecret \
sane \
indicator-kdeconnect-git \
network-manager-applet \
wireguard-tools \
seahorse"

## Fonts
pack="$pack \
ttf-droid \
ttf-liberation \
freefonts \
ttf-dejavu \
ttf-font-icons \
awesome-terminal-fonts \
powerline \
ttf-font-awesome-4"

## Display manager
pack="$pack \
lightdm \
lightdm-webkit2-greeter \
lightdm-webkit-theme-litarvan"

## Theming
pack="$pack \
glpaper-hg \
arc-gtk-theme \
papirus-icon-theme \
python-pywal"

# File Manager
pack="$pack \
thunar \
thunar-volman \
thunar-archive-plugin"

## Im lazy
pack="$pack \
numlockx \
texlive-core \
ffmpeg \
jdk-openjdk \
hunspell-en_US \
ntfs-3g \
libiscsi \
libdvdcss \
libdvdnav \
libdvdread \
gvfs"

## Manjaro
if neofetch | grep -i "manjaro" > /dev/null; then
    echo "Running Manjaro"
    pack="$pack"
fi

## Arch
if neofetch | grep -i "arch" > /dev/null; then
    echo "Running Arch"
    pack="$pack pkgstats"
fi

for package in $pack; do
	#echo "Installing $package"
	if yay -Qv $package > /dev/null; then
		echo "$package is already installed"
	else
		#yay -S $package
		list+=" $package"
	fi
	#echo ""
done

yay -Sv$list
