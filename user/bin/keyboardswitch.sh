#!/bin/sh
if [ $(setxkbmap -print | grep xkb_symbols | awk '{print $4}' | awk -F"+" '{print $2}') = 'us(dvorak)' ]; then
	setxkbmap us;
else
	setxkbmap us dvorak;
fi
exit
