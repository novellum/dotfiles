#!/bin/sh
packages="acpid acpi_call acpica acpi libsysstat sysstat lib32-lm_sensors playerctl"
#acpi is for battery
#mpstat is for cpu usage
#lib32 sensors comes with normal sensors which provide temperature

for package in $packages; do
	echo "Installing $package"
	if pacman -Qs $package > /dev/null; then
		echo "$package is already installed"
	else
		yay $package
	fi
	echo ""
done
