#!/bin/sh
suffix='safe'
file="${1}"
name="${file%.*}"
newname="${name// /_}"
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/printer \ -dNOPAUSE -dQUIET -dBATCH -sOutputFile="$newname"-$suffix.pdf "${1}"
