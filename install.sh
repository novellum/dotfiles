#!/bin/sh

background=https://picsum.photos/id/231/4088/2715
lockscreen=https://picsum.photos/id/368/4896/3264

cd user/Images/wallpaper/background/
if [ $(ls) = $(basename $background) ]; then
    echo "background wallpaper exists"
else
    rm -r background/*
    wget $background
fi
cd ../lockscreen

if [ $(ls) = $(basename $lockscreen) ]; then
    echo "lockscreen wallpaper exists"
else
    rm -r lockscreen/*
    wget $lockscreen
fi

cd ../../../../

cd resources
sh install_packages.sh
sh new_download.sh
cd ../../
#sh dotsroot_download.sh
cd ..

sudo systemctl enable systemd-networkd
sudo systemctl enable wpa_supplicant
sudo systemctl enable systemd-resolved
sudo systemctl disable dhcpcd
sudo systemctl enable NetworkManager
#sudo systemctl enable systemd-tmpfiles-clean

#sudo wpa_supplicant -i $(iw dev | awk '$1=="Interface"{print $2}') -c /etc/wpa_supplicant/wpa_supplicant.conf 
if [[ -f /usr/lib/dhcpcd/dhcpcd-hooks/10-wpa_supplicant ]]; then
    echo "/usr/lib/dhcpcd/dhcpcd-hooks/10-wpa_supplicant exists"
else
    sudo ln -s /usr/share/dhcpcd/hooks/10-wpa_supplicant /usr/lib/dhcpcd/dhcpcd-hooks/
fi

if [[ -d "../secrets/" ]]; then
echo "Secrets already made"
cd ../secrets
git pull origin master
cd ../dotfiles
else
cd ..
git clone https://gitlab.com/novellum/secrets.git
cd secrets
git config credential.helper store
cd ../dotfiles

if [[ -f "../secrets/index/wireguard/$(hostname).conf" ]]; then
    sudo cp -v ../secrets/index/wireguard/$(hostname).conf /etc/wireguard/$(hostname).conf
    sudo systemctl enable wg-quick@$(hostname).service
fi

fi

sudo cp -v ../secrets/root/etc/wpa_supplicant/wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant.conf
