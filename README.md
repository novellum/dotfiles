# Greetings

This is novellum's dotfiles, please enjoy them.

## The View
![Screenshot](dualities.png)

## Installation

To use implement these configuration files, first clone this git repository.

```bash
git clone https://gitlab.com/novellum/dotfiles.git
```

Enter the the dotfiles folder
```bash
cd ~/dotfiles
```

The last step is to run the installation script
```bash
sh install.sh
```

## Usage

These dotfiles do use lightdm as the display manager so it would be preferable to enable it.
```bash
systemctl enable lightdm
```

If your wanting to replace the local version with the updated repository simply use this command.
```bash
gitforce
```
Afterwards run the installation script again.

## License
[MIT](https://choosealicense.com/licenses/mit/)